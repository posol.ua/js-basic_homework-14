/*Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
Основна відмінність між ними полягає в тому, що дані, збережені в LocalStorage, залишаються на пристрої назавжди,
поки їх не видалити вручну або не очистити кеш браузера, тоді як дані, збережені в SessionStorage, 
будуть видалені після закриття вкладки або вікна браузера.

2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
Коли зберігаєш дані в sessionStorage то потрібно розуміти що вони видаляться після закриття/відкриття браузера. 
localStorage зберігає дані доки ти сам їх не видалиш, але дані зберігаються локально на локальній адресі де ти їх зберіг, 
вони не мають можливості надсилатися на сервер, а також при зміні локальної адреси (домен/протокол/порт) ти можеш їх втратити.

3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
Коли завершується сеанс браузера, дані, збережені в sessionStorage, будуть автоматично видалені.

Практичне завдання:
Реалізувати можливість зміни колірної теми користувача.*/


document.addEventListener('DOMContentLoaded', () => {
    const btnTheme = document.getElementById('btnTheme');
    const body = document.body;
    
    if (localStorage.getItem('theme') === 'dark') {
        body.classList.add('dark-theme');
    } else {
        body.classList.add('light-theme');
    }

    btnTheme.addEventListener('click', () => {
        if (body.classList.contains('light-theme')) {
            body.classList.replace('light-theme', 'dark-theme');
            localStorage.setItem('theme', 'dark');
        } else {
            body.classList.replace('dark-theme', 'light-theme');
            localStorage.setItem('theme', 'light');
        }
    });
});
